package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;

/**
 * Created by robotics on 11/9/2017.
 */

@Autonomous(name = "Simple Auton", group = "Sensor")
@Disabled
public class SimpleAuton extends LinearOpMode{
    DcMotor left;
    DcMotor right;

    private ElapsedTime  runtime = new ElapsedTime();

    @Override
    public void runOpMode() throws InterruptedException{

        right = hardwareMap.dcMotor.get("left_drive");
        left = hardwareMap.dcMotor.get("right_drive");

        left.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        right.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        right.setDirection(DcMotor.Direction.REVERSE);

        // Wait for OpMode to be started
        waitForStart();

        // move off the balancing board
        left.setPower(1);
        right.setPower(1);
        Sleep(2);
        left.setPower(0);
        right.setPower(0);
    }
    public void Sleep(double Time)
    {
        runtime.reset();
        while(opModeIsActive() && runtime.seconds() < Time)
        {

        }
    }
}

