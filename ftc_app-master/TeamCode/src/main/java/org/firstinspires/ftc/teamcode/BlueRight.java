package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

/**
 * Created by robotics on 11/9/2017.
 */

@Autonomous(name = "Blue Right Autonomous", group = "Sensor")
public class BlueRight extends LinearOpMode {
    @Override
    public void runOpMode() throws InterruptedException {
        AutonRobot6547 robot = new AutonRobot6547();
        robot.initialize(hardwareMap, this);

        // Wait for OpMode to be started
        waitForStart();

        robot.closeGlyph();

        robot.jewelArmDown();

        Thread.sleep(3000);

        robot.closeGlyph();

        if (robot.isJewelBlue()) {
            robot.rotateJewelBase(2);
        } else {
            robot.rotateJewelBase(0);
        }
        Thread.sleep(4000);
        robot.rotateJewelBase(1);
        robot.intake();
        robot.jewelArmUp();

        robot.raiseGlyph();

        robot.driveForward(20);

        robot.turn(0);

        robot.raiseGlyph();

        robot.POWER = 0.20;

        int pictograph = robot.getPictograph();
        switch (pictograph) {
            case 0:
                robot.driveForward(14);
                break;
            case 1:
                robot.driveForward(21);
                break;
            case 2:
                robot.driveForward(28);
                break;
        }
        robot.turn(83);
        robot.stopIntake();
        robot.POWER = 0.30;
        robot.openGlyph();
        robot.driveForward(10);
        Thread.sleep(500);
        robot.driveForward(-2);
    }
}