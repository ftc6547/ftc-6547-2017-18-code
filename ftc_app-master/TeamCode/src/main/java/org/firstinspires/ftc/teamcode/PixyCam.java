package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.I2cDeviceSynch;


/**
 * Created by Adit Agarwal and Rishi Pathak, FTC 6547 on 11/18/2017.
 */

@Autonomous(name = "PixyOfficialLegoI2C", group = "Lego12c")
//LegoI2C reminds us to set the PixyCam to LegoI2C
public class PixyCam extends LinearOpMode {
    //This program was designed as if we were playing as Blue team. The goal is to hit the read ball
    I2cDeviceSynch pixyCam;


    //ColorSensor leftColorSensor;

    //create reference variable
    double x, y, width, height, numObjects;

    //create DcMotor reference variables
    byte[] pixyData;

    //create an array of bytes to store data that we get from Pixycam
    @Override
    public void runOpMode() throws InterruptedException {
        pixyCam = hardwareMap.i2cDeviceSynch.get("pixy");
        AutonRobot6547 robot = new AutonRobot6547();
        robot.initialize(hardwareMap, this);
        robot.POWER = 0.20;

        //leftColorSensor = hardwareMap.get(ColorSensor.class, "leftColorSensor");

        waitForStart();

        int counter = 0; //creating a counter to measure how many runs pixy has gone
        while (opModeIsActive()) {
            pixyCam.engage();
            pixyData = pixyCam.read(0x51, 5);
            //set pixyData array with length 5

            x = 0xff & pixyData[1];
            //bit x gives x position of the ball
            y = 0xff & pixyData[2];
            //bit y gives y position of the ball
            width = 0xff & pixyData[3];
            //gives width of the ball
            height = 0xff & pixyData[4];
            //gives height of the ball
            numObjects = 0xff & pixyData[0];
            //gives information about the number of objects

            /*telemetry.addLine("Top Left Corner is 0,0");
            telemetry.addData("Number of Objects", 0xff&pixyData[0]);
            telemetry.addData("X Value", 0xff&pixyData[1]);
            telemetry.addData("Y Value", 0xff&pixyData[2]);
            telemetry.addData("Width", 0xff&pixyData[3]);
            telemetry.addData("Height", 0xff&pixyData[4]);
            telemetry.addData("Length of Values", pixyData.length);*/
            //case made if we play as blue team
                //int blue = leftColorSensor.blue();
                //int red = leftColorSensor.red();

                telemetry.addData("X Value of pixy: %s", x);

                if (x<128) {
                    telemetry.addLine("Move back");
                }
                else if(x>128){
                    telemetry.addLine("Move forward");
                }
                telemetry.update();
        }

    }
}