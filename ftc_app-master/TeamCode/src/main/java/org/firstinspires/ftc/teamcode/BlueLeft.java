package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

@Autonomous(name = "Blue Left Autonomous", group = "Sensor")
public class BlueLeft extends LinearOpMode {
    @Override
    public void runOpMode() throws InterruptedException {
        // Create robot class and initialize it
        AutonRobot6547 robot = new AutonRobot6547();
        robot.initialize(hardwareMap, this);

        // Wait for OpMode to be started
        waitForStart();

        // Grab glyph and put arm down
        robot.closeGlyph();

        robot.jewelArmMoreDown();

        Thread.sleep(2000);

        boolean isBlue = robot.isJewelBlue();

        robot.jewelArmDown();

        // Wait 3 seconds for jewel arm to settle down
        Thread.sleep(250);

        // Grab glyph again in case signal didn't go through the first time
        robot.closeGlyph();

        // Rotate the jewel base based on whether or not the left jewel is blue
        if (isBlue) {
            robot.rotateJewelBase(2);
        } else {
            robot.rotateJewelBase(0);
        }

        // Wait a quarter of a second to ensure jewel gets knocked off
        Thread.sleep(4000);

        // Reset jewel base to original position
        robot.rotateJewelBase(1);
        robot.jewelArmUp();

        // Raise glyph grabber up and drive off balance board
        robot.raiseGlyph();
        robot.driveForward(20);

        // Reset angle in case we got mis-aligned coming off balance board
        robot.turn(0);

        // Raise grabber again since its probably fallen down
        robot.raiseGlyph();

        // Increase power since we're off the board and drive another 10 inches to cryptobox
        robot.POWER = 0.15;
        robot.driveForward(10);

        // Rotate so we're looking down the cryptobox
        robot.turn(-90);

        int pictograph = robot.getPictograph();
        // Drive a certain distance along cryptobox based on the pictograph we recorded earlier
        switch (pictograph) {
            case 0:
                robot.driveForward(7);
                break;
            case 1:
                robot.driveForward(14);
                break;
            case 2:
                robot.driveForward(21);
                break;
        }

        // Turn so we're facing cryptobox
        robot.turn(0);
        robot.stopIntake();
        // Increase power to 30% for glyph delivery
        robot.POWER = 0.30;

        // Open grabber and drive 6 inches to place glyph in cryptobox
        robot.openGlyph();
        robot.driveForward(9);

        // Wait half a second and then back 2 inches so we end in safe zone without touching glyph
        Thread.sleep(500);
        robot.driveForward(-2);
    }
}