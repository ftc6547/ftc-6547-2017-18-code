package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

/**
 * Created by robotics on 11/9/2017.
 */

@Autonomous(name = "Test Auton", group = "Sensor")
public class TestAuton extends LinearOpMode {
    @Override
    public void runOpMode() throws InterruptedException {
        AutonRobot6547 robot = new AutonRobot6547();
        robot.initialize(hardwareMap, this);

        // Wait for OpMode to be started
        waitForStart();

        while(opModeIsActive()){
            telemetry.addData("Blue: ", robot.colorSensor.blue());
            telemetry.addData("Red: ", robot.colorSensor.red());
            telemetry.update();
        }
    }
}

