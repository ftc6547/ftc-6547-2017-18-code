package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

@Autonomous(name = "Red Right Autonomous", group = "Sensor")
public class RedRight extends LinearOpMode {
    @Override
    public void runOpMode() throws InterruptedException {
        AutonRobot6547 robot = new AutonRobot6547();
        robot.initialize(hardwareMap, this);

        // Wait for OpMode to be started
        waitForStart();

        robot.closeGlyph();

        robot.jewelArmDown();

        Thread.sleep(3000);

        robot.closeGlyph();

        if (robot.isJewelBlue()) {
            robot.rotateJewelBase(0);
        } else {
            robot.rotateJewelBase(2);
        }
        Thread.sleep(4000);
        robot.rotateJewelBase(1);

        robot.jewelArmUp();

        robot.raiseGlyph();

        robot.intake();

        robot.driveForward(-26);

        robot.turn(0);

        robot.raiseGlyph();

        robot.POWER = 0.15;
        robot.driveForward(-10);
        robot.turn(-90);

        int pictograph = robot.getPictograph();
        switch (pictograph) {
            case 0:
                robot.driveForward(21);
                break;
            case 1:
                robot.driveForward(14);
                break;
            case 2:
                robot.driveForward(7);
                break;
        }
        robot.turn(-173);
        robot.stopIntake();
        robot.POWER = 0.30;
        robot.openGlyph();
        robot.driveForward(9);
        Thread.sleep(500);
        robot.driveForward(-2);
    }
}