package org.firstinspires.ftc.teamcode;

import com.qualcomm.hardware.bosch.BNO055IMU;
import com.qualcomm.hardware.bosch.JustLoggingAccelerationIntegrator;
import com.qualcomm.hardware.modernrobotics.ModernRoboticsI2cRangeSensor;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.navigation.Orientation;
import org.firstinspires.ftc.robotcore.external.navigation.Position;
import org.firstinspires.ftc.robotcore.external.navigation.RelicRecoveryVuMark;
import org.firstinspires.ftc.robotcore.external.navigation.Velocity;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackable;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackables;

/**
 * Created by robotics on 11/9/2017.
 */

@Autonomous(name = "Red Autonomous", group = "Sensor")
public class RedAuton extends LinearOpMode {
    // Declare motors
    DcMotor frontRight;
    DcMotor frontLeft;
    DcMotor backLeft;
    DcMotor backRight;
    DcMotor glyphLeftIntake;
    DcMotor glyphRightIntake;
    DcMotor glyphLinearSlide;

    // Declare servos
    Servo jewelServo;
    Servo glyphRight;
    Servo glyphLeft;

    // Declare various sensors
    BNO055IMU imu;
    Orientation angles;

    ColorSensor ColorSensor;

    ModernRoboticsI2cRangeSensor rangeSensor;

    VuforiaLocalizer vuforia;

    // Calculating conversion to distance for motor encoders
    static final double COUNTS_PER_MOTOR_REV = 1120.0;    // eg: TETRIX Motor Encoder
    static final double WHEEL_DIAMETER_INCHES = 4.0;     // For figuring circumference
    static final double COUNTS_PER_INCH = COUNTS_PER_MOTOR_REV / (WHEEL_DIAMETER_INCHES * 3.14159) / 2;
    double POWER = 0.5;

    // Variable used in sleep method
    private ElapsedTime runtime = new ElapsedTime();

    @Override
    public void runOpMode() throws InterruptedException {
        // Map motors
        frontRight = hardwareMap.dcMotor.get("frontRight");
        frontLeft = hardwareMap.dcMotor.get("frontLeft");
        backRight = hardwareMap.dcMotor.get("backRight");
        backLeft = hardwareMap.dcMotor.get("backLeft");
        glyphRightIntake = hardwareMap.dcMotor.get("glyphMotorRight");
        glyphLeftIntake = hardwareMap.dcMotor.get("glyphMotorLeft");
        glyphLinearSlide = hardwareMap.dcMotor.get("glyphLinearSlide");

        // Map servos
        jewelServo = hardwareMap.servo.get("jewelServo");
        glyphRight = hardwareMap.servo.get("glyphRight");
        glyphLeft = hardwareMap.servo.get("glyphLeft");

        // Map sensors
        imu = hardwareMap.get(BNO055IMU.class, "imu");

        ColorSensor = hardwareMap.get(ColorSensor.class, "leftColorSensor");

        rangeSensor = hardwareMap.get(ModernRoboticsI2cRangeSensor.class, "rangeSensor");

        // Set up parameters for integrated gyro sensor
        BNO055IMU.Parameters gParameters = new BNO055IMU.Parameters();
        gParameters.angleUnit = BNO055IMU.AngleUnit.DEGREES;
        gParameters.accelUnit = BNO055IMU.AccelUnit.METERS_PERSEC_PERSEC;
        gParameters.calibrationDataFile = "BNO055IMUCalibration.json"; // see the calibration sample opmode
        gParameters.loggingEnabled = true;
        gParameters.loggingTag = "IMU";
        gParameters.accelerationIntegrationAlgorithm = new JustLoggingAccelerationIntegrator();
        imu.initialize(gParameters);

        // Set up parameters for vuforia
        int cameraMonitorViewId = hardwareMap.appContext.getResources().getIdentifier("cameraMonitorViewId", "id", hardwareMap.appContext.getPackageName());
        VuforiaLocalizer.Parameters vParameters = new VuforiaLocalizer.Parameters(cameraMonitorViewId);
        vParameters.vuforiaLicenseKey = " AcFfp3T/////AAAAGQEl3ADUUkPShrOvO0GFttBNz33df1X3pMV6cts4GHjMV0cXtqau3w2VSCR14APwFACZi+R+01Sq6sXI4MopeGd5BxgtWPpeCq4WbDbbJvhOm7JSNU/9ORmNAoG1NCGkY1T4xATd+9WPRTfejSdFdVCANc30WT+n/17/6ve0f/MvrbbO72DDjhzURL8Uyh1fG28UWiVCPg/sAm/o31/VzZGhaDVFt6TAUa2+uMAPNbHkbQLpMfEli7xtHzgPr8UpP0sMe3dbvyg/QCDjC5tIALASaRlyhFP0kZWgDVwdCeVQtSk9XhrfVoPu+fP57YQO1h0ZPzb7vWYRKmJE/6iOjvi8o5S82XLRTD4YFuhmve/N";
        vParameters.cameraDirection = VuforiaLocalizer.CameraDirection.BACK;
        this.vuforia = ClassFactory.createVuforiaLocalizer(vParameters);
        VuforiaTrackables relicTrackables = this.vuforia.loadTrackablesFromAsset("RelicVuMark");
        VuforiaTrackable relicTemplate = relicTrackables.get(0);
        relicTemplate.setName("relicVuMarkTemplate"); // can help in debugging; otherwise not necessary

        // Reset encoders for all motors
        frontRight.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        frontLeft.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        backRight.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        backLeft.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

        // Set all motors to run with encoders
        frontRight.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        frontLeft.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        backRight.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        backLeft.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        // Reverse right side motors
        frontRight.setDirection(DcMotorSimple.Direction.REVERSE);
        backRight.setDirection(DcMotorSimple.Direction.REVERSE);

        // Wait for OpMode to be started
        waitForStart();

        // Start tracking data such as gyro angle for imu
        imu.startAccelerationIntegration(new Position(), new Velocity(), 1000);

        // Activate and record vuforia target for later
        relicTrackables.activate();
        RelicRecoveryVuMark vuMark = RelicRecoveryVuMark.from(relicTemplate);

        // Grab glyph that's pre-loaded
        GrabGlyph();

        // Move jewel stick down
        jewelServo.setPosition(1);
        Sleep(1);

        // Lift glyph up
        glyphLinearSlide.setPower(1);
        Sleep(1);
        glyphLinearSlide.setPower(0);

        // Record values from color sensor
        int blue = ColorSensor.blue();
        int red = ColorSensor.red();

        // Move forwards or backwards 3 inches based on color of jewel, the move to safe zone
        if (blue >= red) {
            frontRight.setPower(0.50);
            frontLeft.setPower(0.50);
            backRight.setPower(0.50);
            backLeft.setPower(0.50);
            Sleep(0.20);
            jewelServo.setPosition(0.20);
            frontRight.setPower(0);
            frontLeft.setPower(0);
            backRight.setPower(0);
            backLeft.setPower(0);
            Sleep(0.5);
        } else {
            frontRight.setPower(-0.50);
            frontLeft.setPower(-0.50);
            backRight.setPower(-0.50);
            backLeft.setPower(-0.50);
            Sleep(0.30);
            jewelServo.setPosition(0.20);
            Sleep(1.5);
        }
        frontRight.setPower(0);
        frontLeft.setPower(0);
        backRight.setPower(0);
        backLeft.setPower(0);
    }

    // Drives using encoders. Positive for forward negative for backwards
    public void driveForward(double distance) {
        // Sets target position for each motor based on earlier calculations
        frontRight.setTargetPosition(frontRight.getCurrentPosition() + (int) (distance * COUNTS_PER_INCH));
        frontLeft.setTargetPosition(frontLeft.getCurrentPosition() + (int) (distance * COUNTS_PER_INCH));
        backRight.setTargetPosition(backRight.getCurrentPosition() + (int) (distance * COUNTS_PER_INCH));
        backLeft.setTargetPosition(backLeft.getCurrentPosition() + (int) (distance * COUNTS_PER_INCH));

        // Sets each motor to run to position set above
        frontRight.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        frontLeft.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        backRight.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        backLeft.setMode(DcMotor.RunMode.RUN_TO_POSITION);

        // Sets power to each motor
        frontRight.setPower(POWER);
        frontLeft.setPower(POWER);
        backRight.setPower(POWER);
        backLeft.setPower(POWER);

        // Wait unitl each motor has reached its target position
        while (opModeIsActive() && (Math.abs(frontRight.getTargetPosition() - frontRight.getCurrentPosition()) > 5 || Math.abs(frontLeft.getTargetPosition() - frontLeft.getCurrentPosition()) > 5 || Math.abs(backRight.getTargetPosition() - backRight.getCurrentPosition()) > 5 || Math.abs(backLeft.getTargetPosition() - backLeft.getCurrentPosition()) > 5)) {

        }

        // Stop moving once they have
        frontRight.setPower(0);
        frontLeft.setPower(0);
        backRight.setPower(0);
        backLeft.setPower(0);

        // Set motors back to how they used to be
        frontRight.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        frontLeft.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        backRight.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        backLeft.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
    }

    // Makes accurate turns using buil-in gyro sensor in rev hub
    public void turn(float target) throws InterruptedException {
        // Sets angles variable to values from imu
        angles = imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);

        // Creates variable with just robot angle
        float angleZ = angles.firstAngle;

        // Initializes power to 10%
        double power = 0.50;

        // Keeps looping while robot is 5 degrees away from target angle
        while (opModeIsActive() && Math.abs(target - angleZ) > 5) {
            // Keep updating variables
            angles = imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
            angleZ = angles.firstAngle;

            // Reduces speed to 5% is robot is within 20% of target angle
            if (Math.abs(target - angleZ) < 20) {
                power = 0.40;
            }

            // Determines which way robot should rotate for shortest path
            if (target > angleZ) {
                frontRight.setPower(power);
                backRight.setPower(power);
                frontLeft.setPower(-power);
                frontRight.setPower(-power);
            } else {
                frontRight.setPower(-power);
                backRight.setPower(-power);
                frontLeft.setPower(power);
                frontRight.setPower(power);
            }
        }

        // Stop moving once loop is exited
        frontRight.setPower(0);
        backRight.setPower(0);
        frontLeft.setPower(0);
        frontRight.setPower(0);
    }

    // Smarter sleep method
    public void Sleep(double Time) {
        runtime.reset();
        while (opModeIsActive() && runtime.seconds() < Time) {

        }
    }

    // Closes left and right servo to grab glyph
    public void GrabGlyph() {
        glyphRight.setPosition(0.38);
        glyphLeft.setPosition(0.47);
    }

    // Opens left and right servo to release glyph
    public void OpenGlyph() {
        glyphRight.setPosition(0.65);
        glyphLeft.setPosition(0.33);
    }
}