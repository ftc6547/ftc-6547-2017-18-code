package org.firstinspires.ftc.teamcode;

import com.qualcomm.hardware.modernrobotics.ModernRoboticsI2cGyro;
import com.qualcomm.hardware.modernrobotics.ModernRoboticsI2cRangeSensor;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.I2cDeviceSynch;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.navigation.DistanceUnit;
import org.firstinspires.ftc.robotcore.external.navigation.RelicRecoveryVuMark;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackable;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackables;

/**
 * Created by robotics on 2/1/2018.
 */

public class AutonRobot6547 extends LinearOpMode {
    // Create callingOpMode for printing telemetry
    private OpMode callingOpMode;

    // Declare motors
    public DcMotor frontRight, frontLeft, backRight, backLeft, glyphLinearSlide, glyphLeftIntake, glyphRightIntake;

    // Declare servos
    private Servo glyphRight, glyphLeft, jewelArm, jewelBase;

    // Declare sensors
    public com.qualcomm.robotcore.hardware.ColorSensor colorSensor;
    private ModernRoboticsI2cRangeSensor rightRangeSensor, backRangeSensor;
    private ModernRoboticsI2cGyro gyroSensor;
    I2cDeviceSynch pixyCam;

    // Declare variable to hold vuforia target
    private VuforiaTrackable relicTemplate;
    RelicRecoveryVuMark vuMark = RelicRecoveryVuMark.UNKNOWN;
    private boolean pictoFound = false;

    // Calculating conversion to distance for motor encoders
    private static final double COUNTS_PER_MOTOR_REV = 1120.0;    // eg: TETRIX Motor Encoder
    private static final double WHEEL_DIAMETER_INCHES = 4.0;     // For figuring circumference
    private static final double COUNTS_PER_INCH = COUNTS_PER_MOTOR_REV / (WHEEL_DIAMETER_INCHES * 3.14159) / 2;
    public double POWER = 0.05;

    // Variable to hold pixy values
    byte[] pixyData;

    // Method required since we extend LinearOpMode
    @Override
    public void runOpMode() throws InterruptedException {
    }

    // Called at beginning to set motors, servos sensors, calibrate gyro, etc.
    public void initialize(HardwareMap hardwareMap, OpMode callingOpMode) {
        // Set hardware map to caller's to initialize motors, servos, and sensors
        this.hardwareMap = hardwareMap;

        // Set callingOpMode so we can print telemetry to it
        this.callingOpMode = callingOpMode;

        // Map motors
        frontRight = hardwareMap.dcMotor.get("frontRight");
        frontLeft = hardwareMap.dcMotor.get("frontLeft");
        backRight = hardwareMap.dcMotor.get("backRight");
        backLeft = hardwareMap.dcMotor.get("backLeft");
        glyphLinearSlide = hardwareMap.dcMotor.get("glyphLinearSlide");
        glyphRightIntake = hardwareMap.dcMotor.get("glyphMotorRight");
        glyphLeftIntake = hardwareMap.dcMotor.get("glyphMotorLeft");

        // Map servos
        glyphRight = hardwareMap.servo.get("glyphRight");
        glyphLeft = hardwareMap.servo.get("glyphLeft");
        jewelArm = hardwareMap.servo.get("jewelArm");
        jewelBase = hardwareMap.servo.get("jewelBase");

        // Map sensors
        colorSensor = hardwareMap.get(com.qualcomm.robotcore.hardware.ColorSensor.class, "leftColorSensor");
        pixyCam = hardwareMap.i2cDeviceSynch.get("pixy");
        gyroSensor = hardwareMap.get(ModernRoboticsI2cGyro.class, "gyroSensor");
        rightRangeSensor = hardwareMap.get(ModernRoboticsI2cRangeSensor.class, "rightRangeSensor");
        backRangeSensor = hardwareMap.get(ModernRoboticsI2cRangeSensor.class, "backRangeSensor");

        // Calibrate gyro
        callingOpMode.telemetry.log().add("Gyro Calibrating. Do Not Move!");
        gyroSensor.calibrate();

        // Set up parameters for vuforia
        int cameraMonitorViewId = hardwareMap.appContext.getResources().getIdentifier("cameraMonitorViewId", "id", hardwareMap.appContext.getPackageName());
        VuforiaLocalizer.Parameters vParameters = new VuforiaLocalizer.Parameters(cameraMonitorViewId);
        vParameters.vuforiaLicenseKey = " AcFfp3T/////AAAAGQEl3ADUUkPShrOvO0GFttBNz33df1X3pMV6cts4GHjMV0cXtqau3w2VSCR14APwFACZi+R+01Sq6sXI4MopeGd5BxgtWPpeCq4WbDbbJvhOm7JSNU/9ORmNAoG1NCGkY1T4xATd+9WPRTfejSdFdVCANc30WT+n/17/6ve0f/MvrbbO72DDjhzURL8Uyh1fG28UWiVCPg/sAm/o31/VzZGhaDVFt6TAUa2+uMAPNbHkbQLpMfEli7xtHzgPr8UpP0sMe3dbvyg/QCDjC5tIALASaRlyhFP0kZWgDVwdCeVQtSk9XhrfVoPu+fP57YQO1h0ZPzb7vWYRKmJE/6iOjvi8o5S82XLRTD4YFuhmve/N";
        vParameters.cameraDirection = VuforiaLocalizer.CameraDirection.BACK;
        VuforiaLocalizer vuforia = ClassFactory.createVuforiaLocalizer(vParameters);
        VuforiaTrackables relicTrackables = vuforia.loadTrackablesFromAsset("RelicVuMark");
        relicTemplate = relicTrackables.get(0);
        relicTemplate.setName("relicVuMarkTemplate"); // can help in debugging; otherwise not necessary

        // Reset encoders for all motors
        frontRight.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        frontLeft.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        backRight.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        backLeft.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        glyphLinearSlide.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

        // Set all motors to run with encoders
        frontRight.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        frontLeft.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        backRight.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        backLeft.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        glyphLinearSlide.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        // Reverse left side motors
        frontLeft.setDirection(DcMotorSimple.Direction.REVERSE);
        backLeft.setDirection(DcMotorSimple.Direction.REVERSE);

        // Activate and record vuforia target for later
        relicTrackables.activate();

        pixyCam.engage();

        // Wait while gyro finishes calibrating
        while (!Thread.interrupted() && gyroSensor.isCalibrating()) {
            callingOpMode.telemetry.addLine("Gyro Calibrating. Do not move");
            callingOpMode.telemetry.update();
        }

        callingOpMode.telemetry.addLine("Gyro done calibrating");
        callingOpMode.telemetry.update();
    }

    // Method to drive distance in inches with encoders
    public void driveForward(double distance) {
        // Sets target position for each motor based on earlier calculations
        frontRight.setTargetPosition(frontRight.getCurrentPosition() + (int) (distance * COUNTS_PER_INCH));
        frontLeft.setTargetPosition(frontLeft.getCurrentPosition() + (int) (distance * COUNTS_PER_INCH));
        backRight.setTargetPosition(backRight.getCurrentPosition() + (int) (distance * COUNTS_PER_INCH));
        backLeft.setTargetPosition(backLeft.getCurrentPosition() + (int) (distance * COUNTS_PER_INCH));

        // Sets each motor to run to position set above
        frontRight.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        frontLeft.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        backRight.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        backLeft.setMode(DcMotor.RunMode.RUN_TO_POSITION);

        // Sets power to each motor
        frontRight.setPower(POWER);
        frontLeft.setPower(POWER);
        backRight.setPower(POWER);
        backLeft.setPower(POWER);

        ElapsedTime runtime = new ElapsedTime();

        // Wait until each motor has reached its target position
        while (!Thread.interrupted() && (Math.abs(frontRight.getTargetPosition() - frontRight.getCurrentPosition()) > 5 || Math.abs(frontLeft.getTargetPosition() - frontLeft.getCurrentPosition()) > 5 || Math.abs(backRight.getTargetPosition() - backRight.getCurrentPosition()) > 5 || Math.abs(backLeft.getTargetPosition() - backLeft.getCurrentPosition()) > 5) && runtime.seconds() < 4) {
            if(!pictoFound && RelicRecoveryVuMark.from(relicTemplate) != RelicRecoveryVuMark.UNKNOWN){
                vuMark = RelicRecoveryVuMark.from(relicTemplate);
                pictoFound = true;
            }
        }
        // Stop moving once they have
        frontRight.setPower(0);
        frontLeft.setPower(0);
        backRight.setPower(0);
        backLeft.setPower(0);

        // Set motors back to how they used to be
        frontRight.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        frontLeft.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        backRight.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        backLeft.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
    }

    // Method to drive a certain distance from wall based on back range sensor
    public void driveBackDistanceFromWall(double target) {
        if (getBackDistance() > target) {
            // Determine which way to go based on current distance and target
            frontRight.setPower(-POWER);
            frontLeft.setPower(-POWER);
            backRight.setPower(-POWER);
            backLeft.setPower(-POWER);
        } else {
            frontRight.setPower(POWER);
            frontLeft.setPower(POWER);
            backRight.setPower(POWER);
            backLeft.setPower(POWER);
        }

        // Keep driving till we're within 3 inches from target
        while (Math.abs(getBackDistance() - target) > 3) {

        }

        // Stop driving
        frontRight.setPower(0);
        frontLeft.setPower(0);
        backRight.setPower(0);
        backLeft.setPower(0);
    }

    // Method to drive a certain distance from wall based on right range sensor
    public void driveRightDistanceFromWall(double target) {
        // Figure out which way to strafe based on current distance and target distance
        if (getRightDistance() > target) {
            frontRight.setPower(-POWER);
            frontLeft.setPower(POWER);
            backRight.setPower(POWER);
            backLeft.setPower(-POWER);
        } else {
            frontRight.setPower(POWER);
            frontLeft.setPower(-POWER);
            backRight.setPower(-POWER);
            backLeft.setPower(POWER);
        }

        // Keep strafing till we're within 3 inches of target
        while (Math.abs(getRightDistance() - target) > 3) {

        }

        // Stop moving
        frontRight.setPower(0);
        frontLeft.setPower(0);
        backRight.setPower(0);
        backLeft.setPower(0);
    }

    // Method to turn bot to an angle position using MR gyro
    public void turn(float target) throws InterruptedException {
        // Creates variable with just robot angle
        float angleZ = gyroSensor.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES).firstAngle;

        // Keeps looping while robot is 3 degrees away from target angle
        while (!Thread.interrupted() && Math.abs(target - angleZ) > 3) {
            // Keep updating variable
            angleZ = gyroSensor.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES).firstAngle;

            // Determines which way robot should rotate for shortest path
            if (target < angleZ) {
                frontRight.setPower(POWER);
                backRight.setPower(POWER);
                frontLeft.setPower(-POWER);
                backLeft.setPower(-POWER);
            } else {
                frontRight.setPower(-POWER);
                backRight.setPower(-POWER);
                frontLeft.setPower(POWER);
                backLeft.setPower(POWER);
            }
        }

        // Stop moving once loop is exited
        frontRight.setPower(0);
        backRight.setPower(0);
        frontLeft.setPower(0);
        backLeft.setPower(0);
    }

    // Smarter sleep method
    public void Sleep(double Time) throws InterruptedException {
        // Create and reset ElapsedTime variable
        ElapsedTime runtime = new ElapsedTime();
        runtime.reset();

        // Keep looping till requested sleep time is not up
        while (!Thread.interrupted() && runtime.seconds() <= Time) {

        }
    }

    // Closes glyph grabber
    public void closeGlyph() {
        // Set both glyph servos to close position
        glyphRight.setPosition(0.05);
        glyphLeft.setPosition(0.50);
    }

    // Opens glyph grabber
    public void openGlyph() {
        // Set both glyph servos to open position
        glyphRight.setPosition(0.29);
        glyphLeft.setPosition(0.33);
    }

    public void intake(){
        glyphLeftIntake.setPower(1);
        glyphRightIntake.setPower(1);
    }

    public void stopIntake(){
        glyphRightIntake.setPower(0);
        glyphLeftIntake.setPower(0);
    }

    // Raise glyph grabber up
    public void raiseGlyph() throws InterruptedException {
        // Set linear slide to half speed
        glyphLinearSlide.setPower(0.5);

        // Run slide up for 1.25 seconds and then stop
        Sleep(1.25);
        glyphLinearSlide.setPower(0);
    }

    // Move jewel arm down
    public void jewelArmDown() {
        jewelArm.setPosition(0.87);
    }

    public void jewelArmMoreDown(){
        jewelArm.setPosition(0.95);
    }

    // Move jewel arm down
    public void jewelArmUp() {
        jewelArm.setPosition(0.40);
    }

    // Rotate jewel to either left (position 0), center (position 1), or right (position2)
    public void rotateJewelBase(int position) {
        // Create switch statement based on where base is supposed to rotate to
        switch (position) {
            // Set servo positions based on parameter passed in
            case 0:
                jewelBase.setPosition(0.74);
                break;
            case 1:
                vuMark = RelicRecoveryVuMark.from(relicTemplate);
                if(vuMark != RelicRecoveryVuMark.UNKNOWN){
                    pictoFound = true;
                }
                break;
            case 2:
                jewelBase.setPosition(0.51);
                break;

            // Set to center if none of the above conditions are met
            default:
                jewelBase.setPosition(0.60);
                break;
        }
    }

    // Return right range sensors distance in inches
    public double getRightDistance() {
        return rightRangeSensor.getDistance(DistanceUnit.INCH);
    }

    // Return back range sensors distance in inches
    public double getBackDistance() {
        return backRangeSensor.getDistance(DistanceUnit.INCH);
    }

    // Get vuforia pictograph returning 0 for left, 1 for center, and 2 for right
    public int getPictograph() {
        // Create switch statement based on target seen
        switch (vuMark) {
            // Return 0, 1, or 2 based on target seen
            case LEFT:
                return 0;
            case CENTER:
                return 1;
            case RIGHT:
                return 2;
            // return center column if nothing seen
            default:
                return 1;
        }
    }

    // Method returning true if jewel on left is blue, false otherwise
    public boolean isJewelBlue() {
        pixyData = pixyCam.read(0x51, 5);
        int pixyX = 0xff & pixyData[1];

        // Our color sensor naturally returns more red than blue so give a margin of error of 3
        if(pixyX > 90){
            return true;
        }
        return false;
    }
}