package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.TouchSensor;
import com.qualcomm.robotcore.util.Range;

/**
 * Created by robotics on 11/12/2017.
 */

@com.qualcomm.robotcore.eventloop.opmode.TeleOp(name="Maximus Teleop", group="6547")
@Disabled
public class MaximusTeleop extends OpMode {

    DcMotor left;
    DcMotor right;
    DcMotor intake;
    DcMotor thrower;
    TouchSensor touchSensor;
    static final double     COUNTS_PER_MOTOR_REV    = 1440 ;    // eg: TETRIX Motor Encoder
    static final double     DRIVE_GEAR_REDUCTION    = 2.0 ;     // This is < 1.0 if geared UP
    static final double     WHEEL_DIAMETER_INCHES   = 4.0 ;     // For figuring circumference
    static final double     COUNTS_PER_INCH         = (COUNTS_PER_MOTOR_REV * DRIVE_GEAR_REDUCTION) /
            (WHEEL_DIAMETER_INCHES * 3.1415);
    static final double     DRIVE_SPEED             = 0.6;
    static final double     TURN_SPEED              = 0.5;
    double target;
    static int counter = 0;
    int counter2 = 2000;
    boolean pushed = false;
    boolean pushed2 = false;
    boolean goingUp = false;
    boolean goingDown = false;
    boolean RB = false;
    @Override
    public void init() {
        intake =  hardwareMap.dcMotor.get("left_motor");
        thrower = hardwareMap.dcMotor.get("right_motor");
        left = hardwareMap.dcMotor.get("tape1");
        right = hardwareMap.dcMotor.get("tape2");
        touchSensor = hardwareMap.touchSensor.get("touchSensor");
        //thrower.setDirection(DcMotorSimple.Direction.REVERSE);
        left.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        right.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        thrower.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
    }

    @Override
    public void loop() {
        //intake.setPower(gamepad2.left_trigger);
        //thrower.setPower(gamepad2 .right_trigger);
        float leftPower = -gamepad1.left_stick_y;
        float rightPower = gamepad1.right_stick_y;
        leftPower = Range.clip(leftPower, -1, 1);
        rightPower = Range.clip(rightPower, -1, 1);
        left.setPower(leftPower * leftPower * leftPower * leftPower * leftPower);
        right.setPower(rightPower * rightPower * rightPower * rightPower * rightPower);
        left.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        left.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.FLOAT);
        right.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        right.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.FLOAT);

        if (gamepad2.left_bumper) {
            pushed = true;
            intake.setPower(-1);
            pushed2 = false;
            counter2 = 0;
        }
        if(gamepad2.left_trigger == 1 ){
            if(intake.getPower() == 0 && counter2 > 60){
                pushed = true;
                intake.setPower(1);
                pushed2 = false;
                counter2 = 0;
            }
            if((intake.getPower() == 1 || intake.getPower() == -1) && counter > 60) {
                intake.setPower(0);
                counter = 0;
                pushed = false;
                pushed2 = true;
            }
        }
        if(pushed)
            counter++;
        if(pushed2)
            counter2++;
        if(gamepad2.right_trigger == 1 && goingUp == false){
            goingUp = true;
            thrower.setPower(1);
        }
        if(goingUp && !touchSensor.isPressed()){
            goingUp = false;
            goingDown = true;
        }
        if(goingDown && touchSensor.isPressed()){
            goingDown = false;
            thrower.setPower(0);
        }
        if(gamepad2.right_bumper){
            RB = true;
            thrower.setPower(1);
        }
        if(!gamepad2.right_bumper && RB){
            RB = false;
            thrower.setPower(0);
        }

        /*if(gamepad2.right_trigger == 1){
            throwBall(1);
        }*/

        /*if ( Math.abs(thrower.getCurrentPosition() - thrower.getTargetPosition()) < 5 ) {
            thrower.setPower(0);
        }*/
        /*if(gamepad2.right_trigger == 1){
            loops++;
            if(loops < 500) {
                thrower.setPower(1);
            } else {
                thrower.setPower(0);
            }.setPower(Math.abs(speed));
        } else {
            loops = 0;
        }*/
       /* intake
        .setPower(gamepad2.left_trigger);
        thrower.setPower(gamepad2.right_trigger);
        left.setPower(-gamepad2.left_stick_y);
        right.setPower(gamepad2.right_stick_y);
        if(gamepad2.left_bumper){
            intake.setPower(-1);
        }*/
        /*if(gamepad2.right_bumper) {
            throwBall(0.1);
        }
        */
    }
    public void throwBall(double rotations) {
        int target = thrower.getCurrentPosition() + (int)Math.round(1440*2*rotations);
        thrower.setTargetPosition(target);
        thrower.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        thrower.setPower(Math.abs(1));
    }
}
