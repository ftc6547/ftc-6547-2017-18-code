package org.firstinspires.ftc.teamcode;

import com.disnodeteam.dogecv.CameraViewDisplay;
import com.disnodeteam.dogecv.detectors.CryptoboxDetector;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;

/**
 * Created by robotics on 3/3/2018.
 */

@Autonomous(name = "DogeCV", group = "hi")
public class dogeCV extends OpMode {
    CryptoboxDetector cryptoboxDetector = null;

    @Override
    public void init() {

        telemetry.addLine("Status: Initialized");

        cryptoboxDetector = new CryptoboxDetector();
        cryptoboxDetector.init(hardwareMap.appContext, CameraViewDisplay.getInstance());
        //CameraDevice.getInstance().setFlashTorchMode(true) ;
        cryptoboxDetector.rotateMat = true;
        //cryptoboxDetector.useImportedImage = true;
        //cryptoboxDetector.SetTestMat
        cryptoboxDetector.enable();
    }

    @Override
    public void loop() {

        telemetry.addData("Is Cryptobox Detected ", cryptoboxDetector.isCryptoBoxDetected());
        telemetry.addData("is Column Detected ", cryptoboxDetector.isColumnDetected());
        telemetry.addData("Column Left ", cryptoboxDetector.getCryptoBoxLeftPosition());
        telemetry.addData("Column Center ", cryptoboxDetector.getCryptoBoxCenterPosition());
        telemetry.addData("Column Right ", cryptoboxDetector.getCryptoBoxRightPosition());
    }

    @Override
    public void stop() {
        cryptoboxDetector.disable();
    }
}

