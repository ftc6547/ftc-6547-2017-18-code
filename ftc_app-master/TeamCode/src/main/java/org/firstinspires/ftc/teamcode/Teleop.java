package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.qualcomm.robotcore.util.Range;

/**
 * Created by robotics on 2/5/2018.
 */

@com.qualcomm.robotcore.eventloop.opmode.TeleOp(name = "Teleop", group = "6547")
public class Teleop extends OpMode {
    // Declare motors
    private DcMotor frontRight, frontLeft, backRight, backLeft, glyphLeftIntake, glyphRightIntake, glyphLinearSlide, relicLinearSlide;

    // Declare servos
    private Servo glyphRight, glyphLeft, relicGrabber, relicLifter, jewelArm, jewelBase;

    // Declare and initialize variables used throughout teleop
    private double maxPower = 0.40;
    private double glyphLeftPos = 0.33;
    private double glyphRightPos = 0.29;
    private ElapsedTime speedChangeTime = new ElapsedTime();
    private ElapsedTime glyphIntakeTime = new ElapsedTime();
    private ElapsedTime switchModesTimer = new ElapsedTime();
    private boolean isIntake = false;
    private boolean isOuttake = false;
    private boolean isGlyphMode = true;

    @Override
    public void init() {
        // Mapping motors
        frontRight = hardwareMap.dcMotor.get("frontRight");
        frontLeft = hardwareMap.dcMotor.get("frontLeft");
        backRight = hardwareMap.dcMotor.get("backRight");
        backLeft = hardwareMap.dcMotor.get("backLeft");
        glyphRightIntake = hardwareMap.dcMotor.get("glyphMotorRight");
        glyphLeftIntake = hardwareMap.dcMotor.get("glyphMotorLeft");
        glyphLinearSlide = hardwareMap.dcMotor.get("glyphLinearSlide");
        relicLinearSlide = hardwareMap.dcMotor.get("relicLinearSlide");

        // Mapping servos
        glyphRight = hardwareMap.servo.get("glyphRight");
        glyphLeft = hardwareMap.servo.get("glyphLeft");
        relicGrabber = hardwareMap.servo.get("relicGrabber");
        relicLifter = hardwareMap.servo.get("relicLifter");
        jewelArm = hardwareMap.servo.get("jewelArm");
        jewelBase = hardwareMap.servo.get("jewelBase");

        // Setting motors to use encoders
        frontRight.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        frontLeft.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        backRight.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        backLeft.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        // Setting zero power behavior for smooth breaking
        frontRight.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        frontRight.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.FLOAT);
        frontLeft.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        frontLeft.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.FLOAT);
        backRight.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        backRight.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.FLOAT);
        backLeft.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        backLeft.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.FLOAT);

        // Reverse right side motors
        frontRight.setDirection(DcMotorSimple.Direction.REVERSE);
        backRight.setDirection(DcMotorSimple.Direction.REVERSE);
    }

    @Override
    public void loop() {
        // Keep looping through all methods while op mode is active
        driveRobot();
        driveLinearSlides();
        setGlyphGrabberServos();
        setGlyphIntake();
        setRelicGrabberServos();
        setMaxPower();
        switchModes();
        jewelUp();
        //New line here if harrison presses x then it brakes
        if (gamepad1.x){
            frontLeft.setPower(0);
            frontRight.setPower(0);
            backRight.setPower(0);
            backLeft.setPower(0);
        }

        //NEw line lets see if it works
        //Here if lance preses y then the intake system shuts off
        if (gamepad1.y) {
            glyphLeftIntake.setPower(0);
            glyphRightIntake.setPower(0);
        }
        if (gamepad2.y) {
            glyphLeftIntake.setPower(0);
            glyphRightIntake.setPower(0);
        }

        // Update any values printed to screen for drivers to see
        telemetry.update();
    }

    // Sets power to drive motors
    private void driveRobot() {
        // Record values from joysticks and cube it
        double leftStickY = Math.pow(gamepad1.left_stick_y, 3);
        double leftStickX = -Math.pow(gamepad1.left_stick_x, 3);
        double rightStickX = Math.pow(gamepad1.right_stick_x, 3);

        // Set power to each motor and clip it based on the max power
        frontRight.setPower(Range.clip(leftStickY - leftStickX - rightStickX, -maxPower, maxPower));
        frontLeft.setPower(Range.clip(leftStickY + leftStickX + rightStickX, -maxPower, maxPower));
        backRight.setPower(Range.clip(leftStickY + leftStickX - rightStickX, -maxPower, maxPower));
        backLeft.setPower(Range.clip(leftStickY - leftStickX + rightStickX, -maxPower, maxPower) * 1.05);


    }

    // Sets power to linear slides
    private void driveLinearSlides() {
        // Set power to linear slide
        glyphLinearSlide.setPower(-gamepad2.right_stick_y);


        // Set power to relic slide
        relicLinearSlide.setPower(gamepad2.left_stick_y);
    }

    // Sets power to glyph intake/outtake motors
    private void setGlyphIntake() {
        // Toggles intake and outtake on and off
        if (isGlyphMode) {
            // Set intake on/off
            if (gamepad2.right_bumper && glyphIntakeTime.seconds() >= 1) {
                if (isIntake) {
                    glyphRightIntake.setPower(0);
                    glyphLeftIntake.setPower(0);
                    isIntake = false;
                    glyphIntakeTime.reset();
                } else {
                    glyphRightIntake.setPower(1);
                    glyphLeftIntake.setPower(1);
                    isIntake = true;
                    isOuttake = false;
                    glyphIntakeTime.reset();
                }
                // Set outtake on/off
            } else if (gamepad2.left_bumper && glyphIntakeTime.seconds() >= 1) {
                if (isOuttake) {
                    glyphRightIntake.setPower(0);
                    glyphLeftIntake.setPower(0);
                    isOuttake = false;
                    glyphIntakeTime.reset();
                } else {
                    glyphRightIntake.setPower(-1);
                    glyphLeftIntake.setPower(-1);
                    isOuttake = true;
                    isIntake = false;
                    glyphIntakeTime.reset();
                }




            }
        }
    }

    // Sets servo positions to glyph grabber servos
    private void setGlyphGrabberServos() {
        if (isGlyphMode) {
            // Closes grabber with right trigger
            if (gamepad2.right_trigger == 1) {
                glyphRight.setPosition(1);
                glyphLeft.setPosition(0.55);

                glyphRightPos = 0;
                glyphLeftPos = 0.55;

                // Opens grabber with left trigger
            } else if (gamepad2.left_trigger == 1) {
                glyphRight.setPosition(0.29);
                glyphLeft.setPosition(0.33);

                glyphRightPos = 0.29;
                glyphLeftPos = 0.33;
            }

            // Continues setting positions to servos if closed to apply constant pressure
            if (glyphLeftPos != 0.33) {
                glyphRight.setPosition(glyphRightPos - 1);
                glyphLeft.setPosition(glyphLeftPos + 1);

                glyphRight.setPosition(glyphRightPos);
                glyphLeft.setPosition(glyphLeftPos);
            }
        }
    }

    // Sets positions to servos on relic grabber
    private void setRelicGrabberServos() {
        if (!isGlyphMode) {
            // Closes relic grabber with right trigger
            if (gamepad2.right_trigger == 1) {
                relicGrabber.setPosition(0);

                // Opens relic grabber with left trigger
            } else if (gamepad2.left_trigger == 1) {
                relicGrabber.setPosition(0.75);
            }

            // Lowers relic grabber with right bumper
            if (gamepad2.right_bumper) {
                relicLifter.setPosition(0.30);

                // Swings relic grabber up with left bumper
            } else if (gamepad2.left_bumper) {
                relicLifter.setPosition(1);
            }
        }
    }

    // Sets max driving power for drivers
    private void setMaxPower() {
        // Increase speed by 10% with D-Pad up arrow
        if (gamepad1.right_trigger == 1 && speedChangeTime.seconds() >= 0.5) {
            maxPower += 0.10;
            speedChangeTime.reset();

            // Lower speed by 10% with D-Pad down arrow
        } else if (gamepad1.left_trigger == 1  && speedChangeTime.seconds() >= 0.5) {
            maxPower -= 0.10;
            speedChangeTime.reset();
        }

        if(gamepad1.left_bumper){
            maxPower = 0.20;
        }

        // Limit power between 20% and 50%
        maxPower = Range.clip(maxPower, 0.20, 0.50);

        // Display max speed for drivers to see
        telemetry.addData("Max Speed: %s", maxPower);
    }

    // Switch between glyph and relic mode
    private void switchModes() {
        // Switch between modes if a and b are pressed together
        if (gamepad2.a && gamepad2.b && switchModesTimer.seconds() > 1) {
            isGlyphMode = !isGlyphMode;
            switchModesTimer.reset();
        }

        // Display current mode for drivers to see
        if (isGlyphMode) {
            telemetry.addLine("Glyph Mode");
        } else {
            telemetry.addLine("Relic Mode");
        }
    }

    private void jewelUp(){
        if(gamepad1.right_bumper){
            jewelArm.setPosition(0.40);
            jewelBase.setPosition(0.60);
        }
    }
}
