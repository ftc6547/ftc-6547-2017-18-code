package org.firstinspires.ftc.teamcode;

import com.qualcomm.hardware.bosch.BNO055IMU;
import com.qualcomm.hardware.bosch.JustLoggingAccelerationIntegrator;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.I2cDeviceSynch;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.navigation.Orientation;
import org.firstinspires.ftc.robotcore.external.navigation.Position;
import org.firstinspires.ftc.robotcore.external.navigation.Velocity;

/**
 * Created by robotics on 11/9/2017.
 */

@Autonomous(name = "Blue Autonomous", group = "Sensor")
@Disabled
public class PixyAuton extends LinearOpMode{
    DcMotor left;
    DcMotor right;
    DcMotor LinearSlide;


    Servo GlyphServoLeft;
    Servo GlyphServoRight;
    Servo JewelServo;

    I2cDeviceSynch pixyCam;

    BNO055IMU imu;

    Orientation angles;

    double x, y, width, height, numObjects;
    byte[] pixyData;

    private ElapsedTime  runtime = new ElapsedTime();

    @Override
    public void runOpMode() throws InterruptedException{

        right = hardwareMap.dcMotor.get("left_drive");
        left = hardwareMap.dcMotor.get("right_drive");
        LinearSlide = hardwareMap.dcMotor.get("GlyphSlide");


        GlyphServoLeft = hardwareMap.servo.get("glyphLeft");
        GlyphServoRight = hardwareMap.servo.get("glyphRight");
        JewelServo = hardwareMap.servo.get("jewel");

        pixyCam = hardwareMap.i2cDeviceSynch.get("pixy");

        imu = hardwareMap.get(BNO055IMU.class, "imu");

        BNO055IMU.Parameters parameters = new BNO055IMU.Parameters();
        parameters.angleUnit           = BNO055IMU.AngleUnit.DEGREES;
        parameters.accelUnit           = BNO055IMU.AccelUnit.METERS_PERSEC_PERSEC;
        parameters.calibrationDataFile = "BNO055IMUCalibration.json"; // see the calibration sample opmode
        parameters.loggingEnabled      = true;
        parameters.loggingTag          = "IMU";
        parameters.accelerationIntegrationAlgorithm = new JustLoggingAccelerationIntegrator();
        imu.initialize(parameters);

        left.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        right.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        right.setDirection(DcMotor.Direction.REVERSE);
        LinearSlide.setDirection(DcMotor.Direction.REVERSE);

        pixyCam.engage();
        pixyData = pixyCam.read(0x51, 5);
        // Have pixy read values 3 times so it gets consistent
        for (int i = 0; i < 3; i++)
        {
            x = 0xff&pixyData[1];
            y = 0xff&pixyData[2];
            width = 0xff&pixyData[3];
            height = 0xff&pixyData[4];
            numObjects = 0xff&pixyData[0];
        }

        // Wait for OpMode to be started
        waitForStart();

        imu.startAccelerationIntegration(new Position(), new Velocity(), 1000);

        //GlyphServoLeft.setPosition();

        //THE ZERO VALUE FOR THE SERVO IS 0.18

        // Start moving the servo down
        GlyphServoRight.setPosition(0.42);
        //Sleep(999999);
        JewelServo.setPosition(0.20);

        // Wait for 2.9 seconds while the servo moves
        Sleep(3.3);
        // Stop moving the servo
        JewelServo.setPosition(0.18);

        // Check where the red ball is and move forward or backwards accordingly
        if(x<128){
            left.setPower(-1);
            right.setPower(-1);
        }
        else if(x>128) {

            left.setPower(1);
            right.setPower(1);
        }
        // Move the bot for 1 second to knock the jewel off

        Sleep(1);

        // Stop moving the bot
        left.setPower(0);
        right.setPower(0);

        // Start moving the servo up
        JewelServo.setPosition(0.16);

        // Wait for 1.65 seconds while the servo moves
        Sleep(1.65);
        // Stop moving the servo
        JewelServo.setPosition(0.18);

        // move off the balancing board
        left.setPower(-1);
        right.setPower(-1);
        Sleep(1);

        // turn so we are front on to the cryptobox
        turn(180);

    }
    public void turn(float target) throws InterruptedException{
        angles   = imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
        float angleZ = angles.firstAngle;
        double power = 0.2;
        while(opModeIsActive() && Math.abs(target-angleZ) > 5){
            angles   = imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
            angleZ  = angles.firstAngle;
            if(Math.abs(target-angleZ) < 20)
            {
                power = 0.1;
            }
            if(target > angleZ){
                right.setPower(power);
                left.setPower(-power);
            }
            else{
                right.setPower(-power);
                left.setPower(power);
            }
        }
        right.setPower(0);
        left.setPower(0);
    }
    public void Sleep(double Time)
    {
        runtime.reset();
        while(opModeIsActive() && runtime.seconds() < Time)
        {

        }
    }
}

