package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

/**
 * Created by robotics on 11/9/2017.
 */

@Autonomous(name = "Red Left Autonomous", group = "Sensor")
public class RedLeft extends LinearOpMode {
    @Override
    public void runOpMode() throws InterruptedException {
        AutonRobot6547 robot = new AutonRobot6547();
        robot.initialize(hardwareMap, this);

        // Wait for OpMode to be started
        waitForStart();

        robot.closeGlyph();

        robot.jewelArmMoreDown();

        Thread.sleep(5000);

        boolean isBlue = robot.isJewelBlue();

        robot.jewelArmDown();

        // Wait 3 seconds for jewel arm to settle down
        Thread.sleep(500);

        robot.closeGlyph();

        if (isBlue) {
            //robot.rotateJewelBase(0);
        } else {
            //robot.rotateJewelBase(2);
        }
        Thread.sleep(4000);
        robot.rotateJewelBase(1);

        robot.jewelArmUp();

        robot.intake();

        robot.raiseGlyph();

        robot.driveForward(-20);

        robot.turn(0);

        robot.raiseGlyph();

        robot.POWER = 0.20;

        int pictograph = robot.getPictograph();
        switch (pictograph) {
            case 0:
                robot.driveForward(-25.5);
                break;
            case 1:
                robot.driveForward(-18.5);
                break;
            case 2:
                robot.driveForward(-11.5);
                break;
        }
        robot.turn(83);
        robot.stopIntake();
        robot.POWER = 0.30;
        robot.openGlyph();
        robot.driveForward(10);
        Thread.sleep(500);
        robot.driveForward(-2);
    }
}